
function calcular(){
    let valorAuto = document.getElementById('valorAuto')
    let enganche = document.getElementById('enganche')
    let totFinanciar = document.getElementById('totFinanciar')
    let pagoM = document.getElementById('pagoM')
    let meses = document.getElementById('meses')

    enganche.value = valorAuto.value*.3;

    if(meses.value==12){
        totFinanciar.value = ((valorAuto.value - enganche.value)*1.125)
    }
    if(meses.value == 18){
        totFinanciar.value = ((valorAuto.value - enganche.value)*1.172)
    }
    if(meses.value == 24){
        totFinanciar.value = ((valorAuto.value - enganche.value)*1.21)
    }
    if(meses.value == 36){
        totFinanciar.value = ((valorAuto.value - enganche.value)*1.26)
    }
    if(meses.value == 48){
        totFinanciar.value = ((valorAuto.value - enganche.value)*1.45)
    }
    
    pagoM.value = totFinanciar.value/meses.value
}


function limpiar(){
    
    let valor = document.getElementById('valorAuto')
    let enganche = document.getElementById('enganche')
    let tFinanciar = document.getElementById('totFinanciar')
    let pMensual = document.getElementById('pagoM')


    valor.value = ("")
    enganche.value = ("")
    tFinanciar.value = ("")
    pMensual.value = ("")

}
